from django.conf import settings
from django.core.management import call_command
from django.core.management.base import BaseCommand, CommandError
from django.test.utils import setup_test_environment
from django.test import TestCase


class UpdateDBTest(TestCase):
    def test_updatedb(self):
        # TODO Would normally add things like validating data in DB
        call_command('updatedb')


class Command(BaseCommand):
    def handle(self, *args, **options):
        if not hasattr(settings, "INTEGRATION_TESTS") or not settings.INTEGRATION_TESTS:
            raise CommandError('Please make sure you specified the correct settings file via --settings=integration' +
                               '_test_settings')
        setup_test_environment()
        result = UpdateDBTest('test_updatedb').run()
        # TODO Add parsing of test results
