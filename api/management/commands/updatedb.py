from django.core.management.base import BaseCommand
from api.models import Datapoints
import requests

datasets = ['Tmax', 'Tmin', 'Tmean', 'Sunshine', 'Rainfall']
regions = ['UK', 'England', 'Wales', 'Scotland']
MET_URL = "http://www.metoffice.gov.uk/pub/data/weather/uk/climate/datasets/{}/date/{}.txt"


HEADER_ROW = "Year    JAN    FEB    MAR    APR    MAY    JUN    JUL    AUG    SEP    OCT    NOV    DEC     WIN    SPR" \
             "    SUM    AUT     ANN"
HEADERS = HEADER_ROW.split()


def download_url(url, region, dataset):
    data_lines = []
    past_headers = False
    try:
        response = requests.get(url)
    except requests.exceptions.RequestException:
        return None
    for line in response.iter_lines():
        ln = line.decode("utf-8")
        if ln == '':
            continue
        if past_headers:
            # TODO Should add a regex check here to make sure line contains data
            values = ln.split()
            val_dict = {'region': region, 'dataset': dataset.lower()}
            for (x, value) in enumerate(values):
                key = HEADERS[x]
                if key == "Year":
                    val = int(value)
                else:
                    try:
                        val = float(value)
                    except ValueError:
                        continue
                val_dict[key] = val
            data_lines.append(val_dict)
        if ln == HEADER_ROW:
            past_headers = True
    return data_lines

def upsert(line):
    if not all (k in line for k in ('region', 'dataset', 'Year')):
        return None
    data = Datapoints.objects(region=line['region'], dataset=line['dataset'], Year=line['Year']).update_one(**line,
        upsert=True)
    return data

class Command(BaseCommand):
    help = 'update the database with MET data'

    def handle(self, *args, **options):
        past_headers = False
        for url, region, dataset in [(MET_URL.format(ds, region), region, ds) for ds in datasets for region in regions]:
            self.stdout.write("Downloading {}".format(url))
            data_lines = download_url(url, region, dataset)
            if data_lines is None:
                print("Error downloading from url: " + url)
            for line in data_lines:
                result = upsert(line)
                if result == None:
                    self.stderr.write("Database Error when inserting: {}".format(str(line)))
