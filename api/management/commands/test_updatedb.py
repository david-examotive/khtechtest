from django.test import TestCase
from api.management.commands import updatedb

valid_url = "https://www.metoffice.gov.uk/pub/data/weather/uk/climate/datasets/Tmax/date/Wales.txt"
invalid_url = "x://www.metoffice.gov.uk/pub/data/weather/uk/climate/datasets/Tmax/date/Error"
wales_1942 = {'region': 'Wales', 'dataset': 'tmax', 'Year': 1942, 'JAN': 4.8, 'FEB': 3.7, 'MAR': 8.7, 'APR': 13.0,
              'MAY': 14.7, 'JUN': 18.0, 'JUL': 17.9, 'AUG': 18.8, 'SEP': 16.3, 'OCT': 12.8, 'NOV': 8.1, 'DEC': 9.1,
              'WIN': 5.6, 'SPR': 12.11, 'SUM': 18.24, 'AUT': 12.38, 'ANN': 12.2}


class DownloadUrlTestCase(TestCase):
    def test_download_valid_url(self):
        lines = updatedb.download_url(valid_url, "Wales", "Tmax")
        if wales_1942 not in lines:
            self.fail()

    def test_download_invalid_url(self):
        lines = updatedb.download_url(invalid_url, "Wales", "Tmax")
        self.assertEqual(lines, None)


class UpsertTestCase(TestCase):
    def test_valid_line(self):
        result = updatedb.upsert(wales_1942)
        self.assertIsNotNone(result)

    def test_invalid_line(self):
        result = updatedb.upsert({})
        self.assertEqual(result, None)
