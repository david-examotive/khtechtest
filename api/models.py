from mongoengine import Document, fields

class Datapoints(Document):
    dataset = fields.StringField(required=True)
    region = fields.StringField(required=True)
    Year = fields.IntField(required=True)
    JAN = fields.FloatField()
    FEB = fields.FloatField()
    MAR = fields.FloatField()
    APR = fields.FloatField()
    MAY = fields.FloatField()
    JUN = fields.FloatField()
    JUL = fields.FloatField()
    AUG = fields.FloatField()
    SEP = fields.FloatField()
    OCT = fields.FloatField()
    NOV = fields.FloatField()
    DEC = fields.FloatField()
    WIN = fields.FloatField()
    SPR = fields.FloatField()
    SUM = fields.FloatField()
    AUT = fields.FloatField()
    ANN = fields.FloatField()


