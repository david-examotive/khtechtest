import sys
from rest_framework_mongoengine import viewsets
from api.serializers import DatapointSerializer
from api.models import Datapoints

class DatapointViewSet(viewsets.ModelViewSet):
    lookup_field = 'id'
    serializer_class = DatapointSerializer
    def get_queryset(self):
        filter_dic = {}
        max_year = self.request.query_params.get('max_year', sys.maxsize)
        filter_dic["Year__lte"] = max_year
        min_year = self.request.query_params.get('min_year', 0)
        filter_dic["Year__gte"] = min_year
        region = self.request.query_params.get('region', None)
        if region:
            filter_dic["region__in"] = region.split('|')
        dataset = self.request.query_params.get('dataset', None)
        if dataset:
            filter_dic["dataset__in"] = dataset.split('|')
        return Datapoints.objects(**filter_dic)
