from rest_framework_mongoengine import serializers
from api.models import Datapoints

class DatapointSerializer(serializers.DocumentSerializer):
    class Meta:
        model = Datapoints
        fields = '__all__'