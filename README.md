This uses Django Rest Framework and MongoDB.  Mongoengine is used as a substitute for Django's built-in ORM.
Documents are stored in a single collection.
It Requires MongoDB Running on Localhost (see below)

### Run MongoDB Via the Docker command:
`docker run --name mongo -d -p 27017:27017 mongo`

### The integration settings (integration_test_settings) use a different MongoDB running on port 27018:
`docker run --name mongo_test -d -p 27018:27017 mongo`

### Install dependencies via pip
`pip install -r requirements.txt`

### Download DB Data via:
`python manage.py updatedb`

### Run the API via:
`python manage.py runserver`

### Run Unit Tests via:
`python manage.py test`

### Run Integration Tests via:
`python manage.py integration_tests --settings=integration_test_settings`