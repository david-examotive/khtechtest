import mongoengine

INTEGRATION_TESTS = True

MONGO_CONNECTION = mongoengine.connect(
    db="year_ordered",
    host="localhost",
    port=27018
)

from settings import *
